import React from "react";
import {
  InstantSearch,
  Highlight,
  SearchBox,
  Hits,
  RefinementList,
  Pagination,
  ClearRefinements,
} from "react-instantsearch-dom";
import { instantMeiliSearch } from "@meilisearch/instant-meilisearch";

const searchClient = instantMeiliSearch(
  "http://localhost:14883", // Your MeiliSearch host
  "A53451A4E5A5C21D265944AB8654984016199CCA362F2CA25B3CCD4DF821993B" // Your MeiliSearch API key, if you have set one
);

const Games = () => (
  <InstantSearch
    indexName="Game" // Change your index name here
    searchClient={searchClient}
  >
    <div className="left-panel">
      <ClearRefinements />
      <h2>Genre</h2>
      <RefinementList attribute="genre" />
      <h2>Platform</h2>
      <RefinementList attribute="platform" />
    </div>
    <div className="right-panel">
      <SearchBox />
      <Hits hitComponent={Hit} />

      <div className="pagination">
        <Pagination />
      </div>
    </div>
  </InstantSearch>
);

//const Hit = ({ hit }) => <Highlight attribute="title" hit={hit} />

const Hit = (hit) => {
  const { cover, genre, platform } = hit.hit;
  return (
    <div className="hit media">
      <div className="media-left">
        <img className="media-object" src={`http://localhost:14881/${cover}`} />
      </div>
      <div className="media-body">
        <h4 className="media-heading">
          <Highlight attribute="title" hit={hit.hit} />
        </h4>
        <p className="genre">{genre}</p>
        <p className="platform">{platform}</p>
      </div>
    </div>
  );
};

export default Games;
