# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
# Loads the faker library
require 'faker'

# Deletes existing games, useful if you seed several times
Game.destroy_all

# Creates 1000 fake games
1000.times do
    Game.create!(
        cover: "assets/games/#{rand(1..26)}.jpeg",
        title: Faker::Game.title,
        genre: Faker::Game.genre,
        platform: Faker::Game.platform
    )
end 

# Displays the following message in the console once the seeding is done
puts 'Games created'