class Game < ApplicationRecord
    include MeiliSearch::Rails

    meilisearch force_utf8_encoding: true, primary_key: :id do
        # all attributes will be sent to MeiliSearch if block is left empty
        displayed_attributes ['id', 'cover', 'title', 'genre', 'platform']
        searchable_attributes ['cover', 'title', 'genre', 'platform']
        filterable_attributes ['genre', 'platform']
    end
end